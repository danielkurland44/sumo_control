from traci_utils import SumoSimulationWrapper, create_sumo_cmd
import argparse

MAX_NUMBER_OF_STEPS = 100000
step = 0

parser = argparse.ArgumentParser(description='''Run sumo simulation according to the given parameters''')
parser.add_argument('gui', type=int, default=0, help='Should the simulation run with a graphical representation')
parser.add_argument('cfg_path', type=str, default="", help='path to simulation configurations (.sumocfg file)')
parser.add_argument('trace_path', type=str, default="", help='path to save the simulation output to (as xml file)')
args = parser.parse_args()

sumo_cmd = create_sumo_cmd(args.gui, args.cfg_path, args.trace_path)

sumo_simulation = SumoSimulationWrapper()
sumo_simulation.traci.start(sumo_cmd)
sumo_simulation.generate_junction_dict()
sumo_simulation.print_junction_dict_info("junction_shapes")

while True:
    print("{0} step number {1} {2}".format(50*"*", str(step), 50*"*"))
    try:
        sumo_simulation.simulation_step(is_should_print=False, is_should_plot=False)
    except ConnectionError as ex:
        print("Error in simulating step {step}. The error was:{error}".format(step=step, error=ex))
        break

    if not sumo_simulation.vehicle_dict:
        sumo_simulation.traci.close()
        print("the simulation ended successfully after {step} steps".format(step=step))
        break

    if MAX_NUMBER_OF_STEPS < step:
        print("the simulation reached the maximal number of steps ({step} steps) \n. Stopping simulation."
              .format(step=MAX_NUMBER_OF_STEPS))
        sumo_simulation.traci.close()
        break

    step += 1

