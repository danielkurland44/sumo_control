from shapely.geometry.polygon import Polygon
import matplotlib.pyplot as plt


class Junction(object):
    def __init__(self, junction_id, boundaries, edges={}):
        self.id = junction_id
        self.boundaries = boundaries
        self.bounding_polygon = Polygon(boundaries)
        self.edges = edges

    def get_info_string(self):
        junction_info_string = "[ junction id:{id} | edges:{edges_ids} | boundaries:{boundaries} \n]".format(
            id=self.id,
            edges_ids=self.edges.keys(),
            boundaries=self.boundaries,
        )
        return junction_info_string

    def plot_shape(self):
        x, y = self.bounding_polygon.exterior.xy
        plt.plot(x, y)


class Edge(object):
    def __init__(self, edge_id, boundaries, num_of_lanes):
        self.id = edge_id
        self.num_of_lanes = num_of_lanes
        self.boundaries = boundaries
