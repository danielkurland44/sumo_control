import os
import sys
from junction import Junction
from vehicle import Vehicle
import matplotlib.pyplot as plt
import traci


class SumoSimulationWrapper(object):
    def __init__(self):
        self.traci = traci
        self.vehicle_dict = {}
        self.junction_dict = {}

    def simulation_step(self, is_should_print, is_should_plot):
        self.traci.simulationStep()
        self.update_vehicle_dict()
        if is_should_print:
            self.print_step_info()
        if is_should_plot:
            self.plot_simulation_step()

    def get_vehicle_position(self, vehicle_id, round_to_digits=2):
        return [round(i, round_to_digits) for i in self.traci.vehicle.getPosition(vehicle_id)]

    def generate_junction_dict(self):
        junction_shape_dict = {}
        for junction_id in self.traci.junction.getIDList():
            try:
                if '_' not in junction_id:
                    new_junction = Junction(junction_id, self.traci.junction.getShape(junction_id))
                    junction_shape_dict.update({junction_id: new_junction})
            except ValueError as ex:
                print("failed to get junction:{junction_id} shape! err={exception}".format(junction_id=junction_id,
                                                                                           exception=ex))
        self.junction_dict.update(junction_shape_dict)

    def print_junction_dict_info(self, file_name):
        junction_info_file = None
        if file_name:
            junction_info_file = open(file_name, "w")
        for junction in self.junction_dict.values():
            print(junction.get_info_string())
            if junction_info_file:
                junction_info_file.write(junction.get_info_string())
        if junction_info_file:
            junction_info_file.close()

    def update_vehicle_dict(self):
        current_active_vehicle_ids = self.traci.vehicle.getIDList()
        for vehicle_id in current_active_vehicle_ids:
            x, y = self.get_vehicle_position(vehicle_id)
            new_vehicle = Vehicle(
                vehicle_id=vehicle_id,
                x_coordinate=x,
                y_coordinate=y,
                angle=self.traci.vehicle.getAngle(vehicle_id),
                speed=self.traci.vehicle.getSpeed(vehicle_id),
                acceleration=self.traci.vehicle.getAcceleration(vehicle_id),
                accel=self.traci.vehicle.getAccel(vehicle_id),
                decel=self.traci.vehicle.getDecel(vehicle_id),
                size=0
            )
            self.vehicle_dict.update({vehicle_id: new_vehicle})
        for vehicle_id in list(self.vehicle_dict):
            if vehicle_id not in current_active_vehicle_ids:
                self.vehicle_dict.pop(vehicle_id)

    def print_step_info(self):
        for vehicle in self.vehicle_dict.values():
            vehicle.print_info()
            
    def plot_simulation_step(self):
        for junction in self.junction_dict.values():
            junction.plot_shape()
        for vehicle in self.vehicle_dict.values():
            vehicle.plot_position()
            vehicle.plot_speed_vector()

        plt.draw()
        plt.pause(0.0001)
        plt.clf()


def create_sumo_cmd(gui=False, cfg_path="", trace_path=""):

    if 'SUMO_HOME' in os.environ:
        tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
        sys.path.append(tools)
    else:
        sys.exit("please declare environment variable 'SUMO_HOME'")

    if not cfg_path:
        sys.exit("please provide simulation configuration")

    sumo_binary = "sumo"
    if gui:
        sumo_binary += "-gui"

    sumo_cmd = [sumo_binary, "-c", cfg_path]

    if trace_path:
        sumo_cmd += ["--fcd-output", trace_path]

    return sumo_cmd



