node_types = {
    'priority': 'priority',
    'light': 'traffic_light',
    'no_type': None
}

vehicle_types = [
    {'car': {'accel': '1.0',
             'decel': '5.0',
             'id': 'car',
             'length': '2.0',
             'sigma': '0.9',
             }
     },
    {'bus': {'accel': '1.0',
             'decel': '2.0',
             'id': 'bus',
             'length': '10.0',
             'sigma': '0.5',
             }
     }
]

produced_directory = "x_cfg"
node_xml_name = "x.node.xml"
edge_xml_name = "x.edge.xml"
edge_type_xml_name = "x.type.xml"
net_xml_name = "x.net.xml"
route_xml_name = "x.rou.xml"
simulation_xml_name = "x.sumocfg"


# [node_id, x, y, type]
node_cfg_list = [
    ['n1', '200', '200', node_types['priority']],
    ['n2', '0', '0', node_types['light']],
    ['n3', '400', '0', node_types['light']],
    ['n4', '0', '400', node_types['no_type']],
    ['n5', '400', '400', node_types['no_type']],
]

# [from_node, to_node, num_of_lanes, max_speed]
edge_cfg_list = [
    ['a1', 'n2', 'n1', '3', '45'],
    ['a2', 'n1', 'n2', '3', '45'],
    ['b1', 'n1', 'n4', '2', '15'],
    ['b2', 'n4', 'n1', '2', '15'],
    ['c1', 'n3', 'n1', '2', '30'],
    ['c2', 'n1', 'n3', '2', '30'],
    ['d1', 'n1', 'n5', '3', '30'],
    ['d2', 'n5', 'n1', '3', '30'],
]

# []
route_flow_cfg_list = [
    ['carflow_a', 'car', '0', '1000', '30', 'd1', 'c1'],
    ['carflow_b', 'car', '0', '1000', '30', 'a2', 'b2'],
    ['busflow', 'bus', '0', '1000', '10', 'c1', 'd1'],
]
