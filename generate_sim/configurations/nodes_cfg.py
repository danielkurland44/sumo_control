node_types = {
    'priority': 'priority',
    'light': 'traffic_light',
    'no_type': None
}

vehicle_types = [
    {'car': {'accel': '1.0',
             'decel': '5.0',
             'id': 'car',
             'length': '2.0',
             'sigma': '0.9',
             }
     },
    {'bus': {'accel': '1.0',
             'decel': '2.0',
             'id': 'bus',
             'length': '10.0',
             'sigma': '0.5',
             }
     }
]

produced_directory = "my_cfg"
node_xml_name = "my_node.node.xml"
edge_xml_name = "my_edge.edge.xml"
edge_type_xml_name = "my_type.type.xml"
net_xml_name = "my_net.net.xml"
route_xml_name = "my_route.rou.xml"
simulation_xml_name = "my_simulation.sumocfg"


# [node_id, x, y, type]
node_cfg_list = [
    ['n1', '-500', '0', node_types['priority']],
    ['n2', '-250', '0', node_types['light']],
    ['n3', '-150', '200', node_types['light']],
    ['n4', '0', '0', node_types['no_type']],
    ['n5', '150', '200', node_types['no_type']],
]

# [from_node, to_node, num_of_lanes, max_speed]
edge_cfg_list = [
    ['a', 'n1', 'n2', '3', '45'],
    ['b', 'n2', 'n3', '2', '15'],
    ['c', 'n3', 'n4', '2', '30'],
    ['d', 'n4', 'n5', '3', '30'],
]

# []
route_flow_cfg_list = [
    ['carflow', 'car', '0', '1000', '30', 'a', 'c'],
    ['busflow', 'bus', '0', '1000', '10', 'a', 'd']
]
