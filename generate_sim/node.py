from generate_sim.configurations.x_cfg import node_cfg_list
from generate_sim.general_utils import remove_none_values, convert_attributes_to_sting


class Node(object):
    def __init__(self, node_id, node_x, node_y, node_type):
        self.id = node_id,
        self.x = node_x,
        self.y = node_y,
        self.type = node_type
        remove_none_values(self)
        convert_attributes_to_sting(self)


def generate_node_list_from_cfg():
    node_list = []
    for node_cfg in node_cfg_list:
        node_id, x, y, node_type = node_cfg
        new_node = Node(node_id, x, y, node_type)
        node_list.append(new_node)
    return node_list



