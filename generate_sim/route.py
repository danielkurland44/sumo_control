from generate_sim.configurations.x_cfg import route_flow_cfg_list, vehicle_types
from generate_sim.general_utils import remove_none_values, convert_attributes_to_sting


class VehicleType(object):
    def __init__(self, vehicle_type_id, vehicle_type, length, accel, decel, sigma):
        self.id = vehicle_type_id,
        self.type = vehicle_type,
        self.length = length,
        self.accel = accel,
        self.decel = decel,
        self.sigma = sigma
        remove_none_values(self)
        convert_attributes_to_sting(self)


class Flow(object):
    def __init__(self, flow_id, flow_type, beg, end, number, from_edge, to_edge):
        self.id = flow_id,
        self.type = flow_type,
        self.beg = beg,
        self.end = end,
        self.number = number,
        self.from_edge = from_edge,
        self.to_edge = to_edge
        remove_none_values(self)
        convert_attributes_to_sting(self)


def generate_route_flow_list_from_cfg():
    route_flow_list = []
    for flow in route_flow_cfg_list:
        flow_id, vehicle_type, beg, end, number_of_cars, from_edge, to_edge = flow
        new_flow = Flow(flow_id, vehicle_type, beg, end, number_of_cars, from_edge, to_edge)
        route_flow_list.append(new_flow)
    return route_flow_list


def generate_route_vehicle_type_list_from_cfg():
    vehicle_type_list = []
    for vehicle_type in vehicle_types:
        vehicle_type_name = list(vehicle_type.keys())[0]
        vehicle_type_id = vehicle_type[vehicle_type_name]['id']
        accel = vehicle_type[vehicle_type_name]['accel']
        decel = vehicle_type[vehicle_type_name]['decel']
        length = vehicle_type[vehicle_type_name]['length']
        sigma = vehicle_type[vehicle_type_name]['sigma']
        new_vehicle_type = VehicleType(vehicle_type_id, vehicle_type_name, length, accel, decel, sigma)
        vehicle_type_list.append(new_vehicle_type)
    return vehicle_type_list


