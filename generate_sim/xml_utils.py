from lxml import etree
import fileinput
import re

REPLACE_SRC_LIST = ['from_', 'to_', 'num_of_lanes']
REPLACE_DST_LIST = ['from', 'to', 'NumLanes']

def find_all_occurrences_of_substring(full_string, substring):
    substring = r'(\w*%s\w*)' % substring
    return re.findall(substring, full_string)


def replace_substring_list_in_string(string, substring_list, replacement_string_list):
    assert len(substring_list) == len(replacement_string_list)
    for substring, replacement_string in zip(substring_list, replacement_string_list):
        substring_occurrences = find_all_occurrences_of_substring(string, substring)
        for substring_occurrence in substring_occurrences:
            string = string.replace(substring_occurrence, replacement_string)
    return string


def generate_xml_file_from_class(class_list_dict, xml_file_path, root_name):
    """
    Generate an xml file that describes the received dictionary of classes (the data field is a list of instances of
    the same class).
    Note that if on of the class attributes is 'from' it should be named 'from_', the function would handle it in the
    (*) section.
    :param class_list_dict: A dictionary that has keys according to the desired field_name (class_name) and data
           according to the class attributes.
    :param xml_file_path: The path to which the xml file soul be saved to.
    :param root_name: The xml_root name.
    :return: Nothing
    """
    root = etree.Element(root_name)
    for class_name in class_list_dict.keys():
        class_data_list = class_list_dict[class_name]
        for class_data in class_data_list:
            etree.SubElement(root, class_name, class_data.__dict__)

    etree.ElementTree(root).write(xml_file_path, pretty_print=True)

    # (*) setting field name to 'from' is not possible the way I do it, so patch this solution.
    with fileinput.FileInput(xml_file_path, inplace=True) as file:
        for line in file:
            modified_line = replace_substring_list_in_string(line, REPLACE_SRC_LIST, REPLACE_DST_LIST)
            print(modified_line, end='')

