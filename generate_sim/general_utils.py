
def remove_none_values(class_instance):
    temp_dict = {k: v for k, v in class_instance.__dict__.items() if v is not None}
    class_instance.__dict__.clear()
    class_instance.__dict__.update(temp_dict)


def convert_attributes_to_sting(class_instance):
    for attribute in class_instance.__dict__.keys():
        if class_instance.__dict__[attribute]:
            class_instance.__dict__[attribute] = ''.join(class_instance.__dict__[attribute])
