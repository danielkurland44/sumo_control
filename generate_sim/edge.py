from generate_sim.configurations.x_cfg import edge_cfg_list
from generate_sim.general_utils import remove_none_values, convert_attributes_to_sting


class Edge(object):
    def __init__(self, edge_id, from_node, to_node, num_of_lanes, max_speed):
        self.id = edge_id
        self.from_node = from_node
        self.to_node = to_node
        self.num_of_lanes = num_of_lanes
        self.max_speed = max_speed
        self.type = num_of_lanes + 'L' + max_speed
        remove_none_values(self)
        convert_attributes_to_sting(self)


class EdgeType(object):
    def __init__(self, edge_type_id, num_of_lanes, max_speed, priority=None):
        self.id = edge_type_id
        self.priority = priority
        self.num_of_lanes = num_of_lanes
        self.max_speed = max_speed
        remove_none_values(self)
        convert_attributes_to_sting(self)


def generate_edge_list_from_cfg():
    edge_list = []
    for edge_cfg in edge_cfg_list:
        edge_id, from_node, to_node, num_of_lanes, max_speed = edge_cfg
        new_edge = Edge(edge_id, from_node, to_node, num_of_lanes, max_speed)
        edge_list.append(new_edge)
    return edge_list


def generate_edge_type_list_from_cfg():
    edge_type_list = []
    raw_edge_type_list = set()
    for edge in generate_edge_list_from_cfg():
        raw_edge_type_list.add(edge.type)
    for raw_edge_type in raw_edge_type_list:
        num_of_lanes, max_speed = raw_edge_type.split('L')
        new_edge_type = EdgeType(raw_edge_type, num_of_lanes, max_speed)
        edge_type_list.append(new_edge_type)
    return edge_type_list
