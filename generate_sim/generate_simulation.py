from generate_sim.configurations.x_cfg import produced_directory, node_xml_name, \
    edge_xml_name, edge_type_xml_name, net_xml_name, route_xml_name, simulation_xml_name
from generate_sim.node import generate_node_list_from_cfg
from generate_sim.edge import generate_edge_list_from_cfg, generate_edge_type_list_from_cfg
from generate_sim.route import generate_route_flow_list_from_cfg, generate_route_vehicle_type_list_from_cfg
from lxml import etree
from generate_sim.xml_utils import generate_xml_file_from_class
import os

try:
    # Create target Directory
    os.mkdir(produced_directory)
    print("Directory ", produced_directory,  " Created")
except FileExistsError:
    print("Directory ", produced_directory,  " already exists")

node_xml_path = os.path.join(produced_directory, node_xml_name)
edge_xml_path = os.path.join(produced_directory, edge_xml_name)
edge_type_xml_path = os.path.join(produced_directory, edge_type_xml_name)
net_xml_path = os.path.join(produced_directory, net_xml_name)
route_xml_path = os.path.join(produced_directory, route_xml_name)
simulation_xml_path = os.path.join(produced_directory, simulation_xml_name)

# generate node xml file
generate_xml_file_from_class({"node": generate_node_list_from_cfg()}, node_xml_path, "nodes")

# generate edge xml file
generate_xml_file_from_class({"edge": generate_edge_list_from_cfg()}, edge_xml_path, "edges")

# generate type xml file
generate_xml_file_from_class({"type": generate_edge_type_list_from_cfg()}, edge_type_xml_path, "types")

# generate net file
netconvert_command = "cd {produced_directory} & netconvert --node-files {nodes_path} --edge-files {edges_path} " \
                     "-t {edge_types_path} -o {net_path}".format(produced_directory=produced_directory,
                                                                 nodes_path=node_xml_name,
                                                                 edges_path=edge_xml_name,
                                                                 edge_types_path=edge_type_xml_name,
                                                                 net_path=net_xml_name)
os.system(netconvert_command)

# generate route file
generate_xml_file_from_class({"vType": generate_route_vehicle_type_list_from_cfg(),
                              "flow": generate_route_flow_list_from_cfg()},
                             route_xml_path, "routes")

# generate simulation (.sumocfg)
configuration_root = etree.Element("configuration")
input_root = etree.SubElement(configuration_root, "input")
etree.SubElement(input_root, "net-file", value=net_xml_name)
etree.SubElement(input_root, "route-files", value=route_xml_name)
etree.ElementTree(configuration_root).write(simulation_xml_path, pretty_print=True)

