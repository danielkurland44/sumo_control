from shapely.geometry import Point
import matplotlib.pyplot as plt
import numpy as np


class Vehicle(object):
    def __init__(self, vehicle_id, x_coordinate, y_coordinate, angle, speed, acceleration, accel, decel, size):
        self.id = vehicle_id
        self.x_coordinate = x_coordinate
        self.y_coordinate = y_coordinate
        self.position = Point(x_coordinate, y_coordinate)
        self.angle = angle
        self.speed = speed
        self.acceleration = acceleration
        self.accel = accel
        self.decel = decel
        self.size = size

    def print_info(self):
        vehicle_info_sring = "[ vehicle:{id} | (x, y)=({x},{y}) | speed={speed} | angle={angle} ]".format(
            id=self.id,
            x=self.x_coordinate,
            y=self.y_coordinate,
            speed=self.speed,
            angle=self.angle
        )
        print(vehicle_info_sring)

    def plot_position(self):
        plt.scatter(self.x_coordinate, self.y_coordinate)

    def plot_speed_vector(self):
        if self.speed:
            x_pos = self.x_coordinate
            y_pos = self.y_coordinate
            x_direct = np.sin(self.angle * np.pi / 180.)*self.speed
            y_direct = np.cos(self.angle * np.pi / 180.)*self.speed

            plt.quiver(x_pos, y_pos, x_direct, y_direct)



